/* 
 * Rumeet Goradia (rug5, 177008120) - Section 4
 * Nihar Prabhala (np642, 179005552) - Section 2
 */

package songlib.view;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;

public class SongLibController {

	// Actions enum
	final int NO_ACTION = 0;
	final int ADDING = 1;
	final int EDITING = 2;

	// Actions
	@FXML
	Button addButton;
	@FXML
	Button editButton;
	@FXML
	Button deleteButton;
	@FXML
	Button confirmButton;
	@FXML
	Button cancelButton;
	@FXML
	TextField titleField;
	@FXML
	TextField artistField;
	@FXML
	TextField albumField;
	@FXML
	TextField yearField;
	int currentAction = NO_ACTION;

	// Song Details (Selected Song)
	@FXML
	Label titleDisplay;
	@FXML
	Label artistDisplay;
	@FXML
	Label albumDisplay;
	@FXML
	Label yearDisplay;

	// Song List
	@FXML
	ListView<Song> songListView;
	private ObservableList<Song> songObsList;
	ArrayList<Song> songArrList = new ArrayList<Song>();

	// File path to write in changes to list
	String path;

	public void populateSongList(ArrayList<String> songs) {
		// Sample data, need to figure out how to import from test file
		// songArrList.add(new Song("hello", "goodbye", "hello again", 2019));
		// songArrList.add(new Song("asdjfa", "asdaq", "odfaj", 2014));
		// songArrList.add(new Song("The Morning", "The Weeknd", "House of Balloons",
		// 2011));
		// songArrList.add(new Song("Can't Say", "Travis Scott", "Astroworld", 2018));
		// songArrList.add(new Song("testsong", "testartist2", "testalbum", 2016));
		// songArrList.add(new Song("testsong", "testartist", "testalbum", 2016));
		for (int i = 0; i < songs.size(); ++i) {
			String[] songDetails = songs.get(i).split("\\|");
			// System.out.println(songDetails[0] + "|" + songDetails[1] + "|" +
			// songDetails[2] + "|" + songDetails[3]);
			songArrList.add(new Song(songDetails[0], songDetails[1], songDetails[2],
					songDetails[3].charAt(0) == '-' ? null : Integer.parseInt(songDetails[3])));
		}
		Collections.sort(songArrList);
	}

	public void setFilePath(String path) {
		this.path = path;
	}

	private void disable(boolean disabled) {
		// If argument is true, then everything under the add/edit/delete buttons are
		// disabled
		editButton.setDisable(!disabled);
		deleteButton.setDisable(!disabled);
		addButton.setDisable(!disabled);
		confirmButton.setDisable(disabled);
		cancelButton.setDisable(disabled);
		titleField.setDisable(disabled);
		artistField.setDisable(disabled);
		albumField.setDisable(disabled);
		yearField.setDisable(disabled);
	}

	private void updateFile() {
		try {
			// Clear current songs file
			new FileWriter(path, false).close();
			// Add new list of songs
			FileWriter fw = new FileWriter(path);
			for (int i = 0; i < songObsList.size(); ++i) {
				Song song = songObsList.get(i);
				fw.write(song.toString());
				if (i != songObsList.size() - 1) {
					fw.write("\n");
				}
			}
			fw.close();
		} catch (IOException ioe) {
			System.out.println("An error occurred when updating internal records.");
			ioe.printStackTrace();
		}
	}

	public void add() {
		disable(false);
		currentAction = ADDING;
	}

	public void edit() {
		disable(false);
		songListView.setDisable(true);
		currentAction = EDITING;
		Song selectedSong = songListView.selectionModelProperty().getValue().getSelectedItem();
		// Fill fields with selected song's info
		titleField.setText(selectedSong.getTitle());
		artistField.setText(selectedSong.getArtist());
		albumField.setText(selectedSong.getAlbum());
		yearField.setText(selectedSong.getYear() == null ? "-" : new Integer(selectedSong.getYear()).toString());
	}

	public void delete() {
		Song selectedSong = songListView.selectionModelProperty().getValue().getSelectedItem();
		Alert deleteAlert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete the song '"
				+ selectedSong.getTitle() + "' by " + selectedSong.getArtist() + "?");
		deleteAlert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				int index = songObsList.indexOf(selectedSong);
				songObsList.remove(index);
				// Shift selected song to correct index
				if (songObsList.size() != 0 && songObsList.size() <= index) {
					songListView.getSelectionModel().select(index - 1);
				} else {
					songListView.getSelectionModel().select(index);
				}
				updateFile();
				disableEditAndDelete();
			}
		});
	}

	public void confirm() {
		try {
			Song newSong;
			Alert reqAlert;
			// Ensure that required fields are filled
			if (titleField.getText().isEmpty() && artistField.getText().isEmpty()) {
				reqAlert = new Alert(AlertType.ERROR, "Song title and artist are required.\n");
				reqAlert.showAndWait();
				return;
			} else if (titleField.getText().isEmpty()) {
				reqAlert = new Alert(AlertType.ERROR, "Song title is required.\n");
				reqAlert.showAndWait();
				return;
			} else if (artistField.getText().isEmpty()) {
				reqAlert = new Alert(AlertType.ERROR, "Song artist is required.\n");
				reqAlert.showAndWait();
				return;
			} else {
				// Create new song instance
				if (albumField.getText().isEmpty() && yearField.getText().isEmpty()) {
					newSong = new Song(titleField.getText(), artistField.getText());
				} else if (albumField.getText().isEmpty()) {
					newSong = new Song(titleField.getText(), artistField.getText(),
							Integer.parseInt(yearField.getText()));
				} else if (yearField.getText().isEmpty()) {
					newSong = new Song(titleField.getText(), artistField.getText(), albumField.getText());
				} else {
					newSong = new Song(titleField.getText(), artistField.getText(), albumField.getText(),
							Integer.parseInt(yearField.getText()));
				}
			}

			if (currentAction == ADDING) {
				if (!songArrList.contains(newSong)) {
					songObsList.add(newSong);
					songListView.getSelectionModel().select(newSong);
					FXCollections.sort(songObsList);
					cancel();
					updateFile();
					disableEditAndDelete();
				} else {
					Alert addAlert = new Alert(AlertType.ERROR, "Song with title '" + titleField.getText()
							+ "' and artist '" + artistField.getText() + "' already exists in your library.\n");
					addAlert.showAndWait();
				}
			} else if (currentAction == EDITING) {
				Song oldSong = songListView.selectionModelProperty().getValue().getSelectedItem();
				songObsList.remove(oldSong);
				// Check if title and artist were changed to match an existing song in library
				if (songObsList.contains(newSong)) {
					songObsList.add(oldSong);
					FXCollections.sort(songObsList);
					songListView.getSelectionModel().select(oldSong);
					Alert editAlert = new Alert(AlertType.ERROR,
							"Edit invalid: Another song already exists with title '" + titleField.getText()
									+ "' and artist '" + artistField.getText() + "' in your library.\n");
					editAlert.showAndWait();

				} else {
					songObsList.remove(oldSong);
					songObsList.add(newSong);
					songListView.getSelectionModel().select(newSong);
					FXCollections.sort(songObsList);
					cancel();
					updateFile();
				}
			}
		} catch (NumberFormatException e) {
			// Catch if user didn't enter a valid year
			Alert exceptionAlert = new Alert(AlertType.ERROR, "Please enter a valid (numeric) year.");
			exceptionAlert.showAndWait();
		}
	}

	public void cancel() {
		disable(true);
		disableEditAndDelete();
		songListView.setDisable(false);
		currentAction = NO_ACTION;
		titleField.setText("");
		artistField.setText("");
		albumField.setText("");
		yearField.setText("");
	}

	private void disableEditAndDelete() {
		// If empty list, disable everything
		editButton.setDisable(songObsList.isEmpty());
		deleteButton.setDisable(songObsList.isEmpty());
	}

	public void start(Stage primaryStage) {
		// Set list view
		songObsList = FXCollections.observableArrayList(songArrList);
		disableEditAndDelete();
		songListView.setItems(songObsList);
		// Select first song
		songListView.getSelectionModel().select(0);
		// Customize list view
		songListView.setCellFactory(new Callback<ListView<Song>, ListCell<Song>>() {
			@Override
			public ListCell<Song> call(ListView<Song> s) {
				ListCell<Song> cell = new ListCell<Song>() {
					@Override
					protected void updateItem(Song song, boolean empty) {
						super.updateItem(song, empty);
						if (empty || song == null) {
							setText(null);
						} else {
							setText(song.getTitle() + " (by " + song.getArtist() + ")");
						}
					}
				};
				return cell;
			}
		});
		// "Initialize" song detail displays
		if (songListView.getSelectionModel().getSelectedItem() != null) {
			titleDisplay.setText(songListView.getSelectionModel().getSelectedItem().getTitle());
			artistDisplay.setText(songListView.getSelectionModel().getSelectedItem().getArtist());
			albumDisplay.setText(songListView.getSelectionModel().getSelectedItem().getAlbum());
			yearDisplay.setText(songListView.getSelectionModel().getSelectedItem().getYear() == null ? "-"
					: new Integer(songListView.getSelectionModel().getSelectedItem().getYear()).toString());
		} else {
			titleDisplay.setText("");
			artistDisplay.setText("");
			albumDisplay.setText("");
			yearDisplay.setText("");
		}
		// Create listener for selected song's details
		songListView.getSelectionModel().selectedItemProperty().addListener((obs, oldSong, newSong) -> {
			if (newSong != null) {
				titleDisplay.setText(newSong.getTitle());
				artistDisplay.setText(newSong.getArtist());
				albumDisplay.setText(newSong.getAlbum());
				yearDisplay.setText(newSong.getYear() == null ? "-" : new Integer(newSong.getYear()).toString());
			} else {
				titleDisplay.setText("");
				artistDisplay.setText("");
				albumDisplay.setText("");
				yearDisplay.setText("");
			}
		});
	}

	// public void convert(ActionEvent e) {
	// Button b = (Button)e.getSource();
	// if (b == f2c) {
	// float fval = Float.valueOf(f.getText());
	// float cval = (fval-32)*5/9;
	// c.setText(String.format("%5.1f", cval));
	// } else {
	// float cval = Float.valueOf(c.getText());
	// float fval = cval*9/5+32;
	// f.setText(String.format("%5.1f", fval));
	// }
	// }

	// public void changeList(ActionEvent e) { // Nihar note: methods in our java
	// files are referenced in the fxml files
	// // using for
	// // example, onAction="#methodName" for a button
	// Button b = (Button) e.getSource();
	// // if (b == add) {
	// // // TextInputDialog addSong = new TextInputDialog("songname | year");
	// // // addSong.setTitle("Add Song");
	// // // addSong.setHeaderText("Enter song");
	// // // addSong.setContentText("Song: ");
	// //
	// // // Optional<String> result = addSong.showAndWait();
	// //
	// // // result.ifPresent(song -> {this.b.setText(song));
	// //
	// // // make new Scene
	// // // Stage primaryStage = (Stage) b.getScene().getWindow(); // gets the
	// Stage
	// // that
	// // // Button is hosted on
	// // // //root = FXMLLoader.load(getClass().getResource("addView.fxml")); // so
	// // this
	// // // is where it should pick up the text input dialog
	// // // Scene addScene = new Scene(root);
	// // // primaryStage.setScene(addScene);
	// // // primaryStage.show();
	// // //
	// // System.out.println("The add function was accessed");
	// // // have to get the song name, artist, year (optional), album (optional)
	// // // add this to the list and sort it alphabetically
	// // // check to see if already in list; if it is, disallow and display message
	// // } else if (b == edit) {
	// // // the selected song in the list is the one that has to be edited
	// // // have pop-up editable text field with info for that song and save the
	// // changes
	// // // into the list
	// // // do a check to make sure that this is not already in list; disallow and
	// // // display message
	// // } else if (b == delete) {
	// // // the selected song has to be deleted from the list
	// // // next song in the list should then be automatically selected with
	// details
	// // // displayed
	// // // if no next song, display the previous song in the list
	// // }
	// }
}