/* 
 * Rumeet Goradia (rug5, 177008120) - Section 4
 * Nihar Prabhala (np642, 179005552) - Section 2
 */

package songlib.view;

public class Song implements Comparable<Song> {
	private String title, artist, album;
	private Integer year;

	public Song(String title, String artist, String album, Integer year) {
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.year = year;
	}
	
	public Song(String title, String artist) {
		this(title, artist, "-", null);
	}
	
	public Song(String title, String artist, String album) {
		this(title, artist, album, null);
	}
	
	public Song(String title, String artist, Integer year) {
		this(title, artist, "-", year);
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String newTitle) {
		this.title = newTitle;
	}

	public String getArtist() {
		return this.artist;
	}

	public void setArtist(String newArtist) {
		this.artist = newArtist;
	}

	public String getAlbum() {
		return this.album;
	}

	public void setAlbum(String newAlbum) {
		this.album = newAlbum;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(int newYear) {
		this.year = newYear;
	}

	@Override
	public int compareTo(Song song) {
		if (this.getTitle().toUpperCase().compareTo(song.getTitle().toUpperCase()) == 0) {
			return this.getArtist().toUpperCase().compareTo(song.getArtist().toUpperCase());
		} else {
			return this.getTitle().toUpperCase().compareTo(song.getTitle().toUpperCase());
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			Song song = (Song) obj;
			return this.getTitle().toUpperCase().equals(song.getTitle().toUpperCase()) && this.getArtist().toUpperCase().equals(song.getArtist().toUpperCase());
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return (this.getTitle() + "|" + this.getArtist() + "|" + this.getAlbum() + "|" + (this.getYear() == null ? "-" : this.getYear().toString()));
	}

}
