/* 
 * Rumeet Goradia (rug5, 177008120) - Section 4
 * Nihar Prabhala (np642, 179005552) - Section 2
 */

package songlib.app;

import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import songlib.view.SongLibController;

public class SongLib extends Application {

	public void start(Stage primaryStage) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/songlib/view/SongLib.fxml"));
		Pane root = (Pane) loader.load();

		// Read in data from text file
		String path = System.getProperty("user.dir") + "/src/songlib/app/songs.txt";
		File songsFile = new File(path);
		Scanner sc = new Scanner(songsFile);
		ArrayList<String> songs = new ArrayList<String>();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			int countsep = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == '|') {
					countsep++;
				}
			}
			if (countsep == 3) {
				songs.add(line);
			}
		}
		sc.close();

		// Pass songs into view controller and start
		SongLibController songLibController = loader.getController();
		songLibController.populateSongList(songs);
		songLibController.setFilePath(path);
		songLibController.start(primaryStage);

		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Song Library");
		primaryStage.setResizable(false);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
