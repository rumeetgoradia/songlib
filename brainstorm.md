## Ideas

1. Song class with all necessary fields -- name, artist, album, year
2. Use ArrayList to maintain all song objects, with Comparator for song name and song artist (ans use ObservableList)
3. Store songs in JSON file, easiest way to read songs
4. Any update to songs list = rewrite entire JSON file?
5. When deleting songs, need to maintain index of the song
6. How to get JSON into ArrayList?
7. Maybe use Table View for songs? [First example](https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/table-view.htm#CJAGAAEE) looks pretty cool.
